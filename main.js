const keyboard = document.getElementById('keyboard');
const urlBar   = document.getElementById('urlbar');
const webview  = document.getElementById('webview');

let capslockOn = false;
let shiftOn = false;

for (let i = 0; i < keyboard.children.length; i++) {
	let key = keyboard.children[i];

	let dataKeys = key.dataset.keys;

	if (dataKeys) {
		key.onclick = () => pressKey(key.dataset.keys);
	} else {
		switch (key.id) {
			case "backspace":
				key.onclick = () => backspace();
				break;
			case "tab":
				key.onclick = () => pressKey('  ');
				break;
			case "capslock":
				key.onclick = () => capslock();
				break;
			case "enter":
				key.onclick = () => enterUrl();
				break;
			case "lshift":
			case "rshift":
				key.onclick = () => shift();
				break;
			case "spacebar":
				key.onclick = () => pressKey('  ');
				break;
		}
	}
}

document.getElementById('go-button').onclick = () => enterUrl();

webview.onLoad = updateUrlBar();

function focusUrlBar() {
	urlBar.focus();
}

function pressKey(key) {
	let upper = shiftOn || capslockOn;

	if (upper) {
		urlBar.value += key.slice(1, 2);
	} else {
		urlBar.value += key.slice(0, 1);
	}

	if (shiftOn) shift();

	focusUrlBar();
}

function backspace() {
	let url = urlBar.value;
	urlBar.value = url.slice(0, -1);
	focusUrlBar();
}

function capslock() {
	shiftOn = false;
	document.getElementById('lshift').classList.remove('highlight');
	document.getElementById('rshift').classList.remove('highlight');

	capslockOn = capslockOn ? false : true;
	document.getElementById('capslock').classList.toggle('highlight');

	changeCase();
	focusUrlBar();
}

function shift() {
	capslockOn = false;
	document.getElementById('capslock').classList.remove('highlight');

	shiftOn = shiftOn ? false : true;
	document.getElementById('lshift').classList.toggle('highlight');
	document.getElementById('rshift').classList.toggle('highlight');

	changeCase();
	focusUrlBar();
}

function changeCase() {
	let upper = shiftOn || capslockOn;

	for (let i = 0; i < keyboard.children.length; i++) {
		let key = keyboard.children[i];

		let dataKeys = key.dataset.keys;

		if (dataKeys) {
			if (upper) {
				key.innerHTML = dataKeys.slice(1, 2);
			} else {
				key.innerHTML = dataKeys.slice(0, 1);
			}
		}
	}
}

function toggleKeyboard() {
	keyboard.hidden = keyboard.hidden ? false : true;
	webview.classList.toggle("webview-fullscreen");
}

function enterUrl() {
	const protocolRegex = new RegExp("^.*:.*$");
	let url = urlBar.value;

	if (!url)
		return;

	if (!protocolRegex.test(url))
		url = "https://" + url;

	urlBar.value = webview.src = url;
}

function updateUrlBar() {
	if (webview.src !== "about:blank")
		urlBar.value = webview.src;
}
